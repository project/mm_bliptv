********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: MM BlipTV Module
Author: Patrick Fournier <pfournier at whiskyechobravo dot com>
Drupal: 5
********************************************************************
PREREQUISITES:
 Media Mover

********************************************************************
DESCRIPTION:

This module allows you to upload media files to the blip.tv service.

********************************************************************
INSTALLATION:

1. Place the entire upload_progress directory into your Drupal 
   modules directory (normally sites/all/modules or modules).

2. Enable the upload_progress module by navigating to:

     Administer > Site building > Modules

3. Set up Media Mover appropriately.
   
********************************************************************
